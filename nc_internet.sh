#!/bin/bash

if [ ! -x /usr/bin/nc ] ; then
    # some extra check if wget is not installed at the usual place                                                                           
    command -v nc >/dev/null 2>&1 || { echo >&2 "Please install nc or set it in your path. Aborting."; exit 1; }
fi
# https://stackoverflow.com/questions/14411103/check-for-existence-of-wget-curl

echo -e "GET http://google.com HTTP/1.0\n\n" | nc google.com 80 > /dev/null 2>&1

if [ $? -eq 0 ]; then
    echo "Online"
else
    echo "Offline"
fi

# https://stackoverflow.com/questions/929368/how-to-test-an-internet-connection-with-bash