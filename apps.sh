#!/bin/bash 

# enable firewall / default block incomming signals
### https://www.digitalocean.com/community/tutorials/how-to-setup-a-firewall-with-ufw-on-an-ubuntu-and-debian-cloud-server
### https://www.cyberciti.biz/faq/how-to-configure-firewall-with-ufw-on-ubuntu-20-04-lts/
sudo ufw status;
sudo ufw enable;

# BASIC TOOLS
### BASIC Upgrade with SSL
### This is needed to add repositories and
### Needed for https repositories etc
sudo apt install -y libssl-dev;
sudo apt install -y apt-transport-https;
sudo apt install -y apt-utils;
sudo apt install -y ca-certificates;
sudo apt install -y software-properties-common;
sudo apt install -y gnupg2; 
sudo apt install -y dirmngr; 
sudo apt install -y dpkg; 

## Optional .. this is a dpkg alternative
#### sudo dpkg -i YourDebPackage.deb
#### sudo gdebi YourDebPackage.deb
### https://askubuntu.com/questions/621351/gdebi-vs-dpkg-how-does-gdebi-automatically-gets-missing-dependancies-can-i-u#:~:text=Actually%20gdebi%20is%20just%20a,-get%20-f%20install%20).

sudo apt install -y gdebi;

# Before installing basic tools, add repository
# APT additions

# install Ubuntu Restricted Extras?
# https://itsfoss.com/install-media-codecs-ubuntu/
# https://itsfoss.com/ubuntu-repositories/
sudo add-apt-repository multiverse;

## Needed for htop and other utils
# https://linuxconfig.org/how-to-install-tweak-tool-on-ubuntu-20-04-lts-focal-fossa-linux
# https://itsfoss.com/ubuntu-repositories/
sudo add-apt-repository universe;

### BASIC Get and Edit files
## https://www.cyberciti.biz/faq/howto-install-vim-on-ubuntu-linux/
## http://www.thegeekstuff.com/2008/10/midnight-commander-mc-guide-powerful-text-based-file-manager-for-unix/
## https://www.cyberciti.biz/tips/midnightcommander-set-vi-as-default-editor-viewer.html
sudo apt install -y mc nano sed vim;

### BASIC Internals monitoring
sudo apt install -y htop;
sudo apt install -y atop;
sudo apt install -y iotop;
sudo apt install -y lm-sensors;

# overpowerd but a rising star: Glances
## https://github.com/nicolargo/glances/blob/master/README.rst
## tip: install python3-dev , python3-pip, sudo pip install glances

#### BASIC network tooling
sudo apt install -y net-tools;
sudo apt install -y ethtool;
sudo apt install dnsutils -y;
sudo apt install -y curl wget rsync;

#### BASIC terminal browser
sudo apt install -y lynx;

#### BASIC unpacker
sudo apt install -y zip;

#### run commands and then detach and reattach later
sudo apt install -y screen;
sudo apt install -y tmux;

#### Disk usage information
sudo apt install -y ncdu;
# sudo apt install -y duf;
sudo apt install -y baobab; 

#### Possible extra tools
# sudo apt install bmon iftop slurm;
# sudo apt install uptimed;
# sudo apt install neofetch screenfetch;
# sudo apt install gnome-disk-utility;

# VPN
## Download and install nordvpn
sh <(curl -sSf https://downloads.nordcdn.com/apps/linux/install.sh);


#### configure Powermanagement
## https://wiki.archlinux.org/index.php/Laptop_Mode_Tools
# sudo apt install -y laptop-mode-tools;
## Laptop_Mode_Tools appears to be superpassed by tlp

# tlp
## Optional
### https://ubuntuhandbook.org/index.php/2020/06/improve-battery-life-ubuntu-20-04-lts/
### https://sourcedigit.com/25072-how-to-install-tlp-to-improve-battery-life-of-ubuntu/
### https://vitux.com/improving-battery-life-in-ubuntu-with-tlp/
### https://wiki.archlinux.org/index.php/TLP
### https://linrunner.de/tlp/
# sudo add-apt-repository ppa:linrunner/tlp-beta

# Tip: add-repository does auto-update. and install tlp does autoremove Laptop_Mode_Tools
sudo add-apt-repository ppa:linrunner/tlp;
sudo apt install -y tlp tlp-rdw;
sudo tlp-stat -s;


## Video driver apt
# https://launchpad.net/~oibaf/+archive/ubuntu/graphics-drivers
# https://linuxconfig.org/amd-radeon-ubuntu-20-04-driver-installation
# https://linuxconfig.org/benchmark-your-graphics-card-on-linux
# https://www.phoronix-test-suite.com/?k=downloads
sudo add-apt-repository ppa:oibaf/graphics-drivers;
sudo apt install -y mesa-utils;


## REINSTALL MESA
# https://askubuntu.com/questions/1102431/reinstall-mesa-driver
# https://www.itsfoss.net/how-to-install-mesa-drivers-on-ubuntu/
# https://itsfoss.com/install-mesa-ubuntu/
# https://linuxconfig.org/install-and-test-vulkan-on-linux


## Browsers

### Firefox
# https://www.ubuntuupdates.org/ppa/ubuntu_mozilla_security
sudo add-apt-repository ppa:ubuntu-mozilla-security/ppa;

### Opera
# https://deb.opera.com/manual.html
# https://wiki.debian.org/Opera
# research if needed, install adds double apt file?
echo "deb https://deb.opera.com/opera-stable/stable non-free" | sudo tee /etc/apt/sources.list.d/opera.list;
wget -O - https://deb.opera.com/archive.key | sudo apt-key add -;

### Chrome
#### Not needed. install via app-store adds repository?
#wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -;
#sudo sh -c 'echo "deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list';

## Multi Media

### libreoffice
# https://www.liquidweb.com/kb/installing-libreoffice-on-ubuntu-16-04/
sudo add-apt-repository ppa:libreoffice/ppa;

### GIMP
# https://medium.com/@chillypenguin/chromebook-linux-apps-for-web-developers-902186f49b60
# https://launchpad.net/~otto-kesselgulasch/+archive/ubuntu/gimp __> thorsten needs time
# https://launchpad.net/~ubuntuhandbook1/+archive/ubuntu/gimp
# sudo add-apt-repository ppa:otto-kesselgulasch/gimp;
# sudo add-apt-repository ppa:ubuntuhandbook1/gimp
# sudo apt install ppa-purge
# sudo ppa-purge ppa:otto-kesselgulasch/gimp
# sudo ppa-purge ppa:ubuntuhandbook1/gimp
sudo apt install gimp -y;



### Spotify
# https://www.spotify.com/de/download/linux/
echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list;
curl -sS http://download.spotify.com/debian/pubkey.gpg | sudo apt-key add - ;

### Skype
# dpkg -s apt-transport-https > /dev/null;
# https://linuxconfig.org/how-to-install-skype-in-ubuntu-20-04-focal-fossa-linux
# repo not needed / snap is used
# curl https://repo.skype.com/data/SKYPE-GPG-KEY | sudo apt-key add -;
# echo "deb [arch=amd64] https://repo.skype.com/deb stable main" | sudo tee /etc/apt/sources.list.d/skype-stable.list;

### VLC
# https://www.videolan.org/vlc/download-ubuntu.html
# repo not needed / snap is used
# sudo add-apt-repository ppa:videolan/stable-daily;

## Development

### Sublime
#### https://medium.com/@chillypenguin/chromebook-linux-apps-for-web-developers-902186f49b60
#### https://www.sublimetext.com/docs/3/linux_repositories.html
# repo not needed / snap is used
# wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -;
# sudo apt-add-repository "deb https://download.sublimetext.com/ apt/stable/";
# https://packagecontrol.io/packages/Sync%20Settings
## Sync Settings via Github Gist
#### List of nice packages (tip include latex)
#https://jdhao.github.io/2018/03/10/sublime-text-latextools-setup/
## https://github.com/randy3k/Terminus


# Virtual Box
#### not ready for focal (20.04)
echo "deb https://download.virtualbox.org/virtualbox/debian '$(lsb_release -cs)' contrib non-free" | sudo tee /etc/apt/sources.list.d/virtualbox.list;
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -;
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -;

### Ubuntu Partner
echo "deb https://archive.canonical.com/ $(lsb_release -sc) partner" | sudo tee /etc/apt/sources.list.d/ubuntu-partner.list;

# Bash Commands
## Version information
cat /proc/version;
cat /etc/*release;
sudo lsb_release -a;

## Clean upgrade
sudo apt clean -yq;
sudo apt autoclean -yq;
sudo apt autoremove -yq;

sudo apt update;
sudo apt upgrade -y;
sudo apt dist-upgrade -y;

sudo apt clean -yq;
sudo apt autoclean -yq;
sudo apt autoremove -yq;

## Tools

### BASIC Build and version files
sudo apt install -y build-essential;
sudo apt install -y dkms;
sudo apt install -y git git-core;
sudo apt install -y gdebi-core;

### Code Editors

## https://www.sublimetext.com/
## https://github.com/sublimehq
## https://www.sublimetext.com/docs/3/linux_repositories.html
### sudo apt install -yqq sublime-text;
sudo snap install -y sublime-text --classic;

sudo snap install -y atom --classic;
sudo apt install -y sublime-merge;

## https://wiki.archlinux.org/index.php/Neovim
## https://packages.ubuntu.com/search?keywords=neovim
## https://github.com/neovim/neovim
## https://neovim.io/
### sudo apt install -y neovim;
sudo snap install -y nvim --classic

#### Mainline kernel installation tool
## https://ubuntuhandbook.org/index.php/2020/08/mainline-install-latest-kernel-ubuntu-linux-mint/
sudo add-apt-repository ppa:cappelikan/ppa;
sudo apt update -y;
sudo apt install mainline -y;

# sudo apt remove mainline
# sudo add-apt-repository --remove ppa:cappelikan/ppa


### VirtualBox 
### alternative
# sudo gdebi virtualbox-6.1_6.1.16-140961_Ubuntu_eoan_amd64.deb
sudo apt install -y virtualbox;

### VOD and codecs

# install ffmpeg tool
sudo apt install -y ffmpeg;

# install AC-3 Codec (audio)
## https://en.wikipedia.org/wiki/Dolby_Digital
sudo apt install -y ogmrip-ac3;

# install  H.264 codec
## https://en.wikipedia.org/wiki/Advanced_Video_Coding 
# install h.265 codec
## https://en.wikipedia.org/wiki/High_Efficiency_Video_Coding
sudo apt install -y libavcodec58;

# https://linuxconfig.org/watch-netflix-on-ubuntu-18-04-bionic-beaver-linux
# Depending on your Ubuntu 18.04 installation you will need to install extra codes
sudo apt install -y libavcodec-extra;

# This installs to much stuff and is old, but it works. 
## TODO see if steps above are enough to play 
# https://itsfoss.com/install-media-codecs-ubuntu/
# https://linuxhint.com/install_multimedia_codecs_ubuntu/ <-- test video's 
sudo apt install -y ubuntu-restricted-extras;

### Ubuntu Tools
## Deprecated
# sudo apt install -y unity-tweak-tool;

## Gnome tweak tool
# https://linuxconfig.org/how-to-install-tweak-tool-on-ubuntu-20-04-lts-focal-fossa-linux
sudo apt install -y gnome-tweak-tool;

## Gnome extensions tool
# https://linuxconfig.org/how-to-install-gnome-shell-extensions-on-ubuntu-20-04-focal-fossa-linux-desktop
sudo apt install -y gnome-shell-extensions;

### Multimedia 
sudo apt install -y gimp;
sudo apt remove  -y libreoffice;
sudo apt install -y libreoffice;
sudo apt install -y spotify-client;
sudo snap install vlc;


#### Multimedia -= Video editting
sudo apt install -y openshot;
sudo apt install -y shotcut;

### Browers
# https://linuxconfig.org/how-to-install-google-chrome-web-browser-on-ubuntu-20-04-focal-fossa
# wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb;
# sudo gdebi google-chrome-stable_current_amd64.deb;

sudo apt install -y torbrowser-launcher;
sudo apt install -y tor-browser;

sudo apt install -y google-chrome-stable;
sudo apt install -y firefox;
sudo apt install -y opera-stable;

sudo apt install -y chromium-browser;

# sudo apt install -y skypeforlinux;
sudo snap install skype --classic;

# Optional Powermanagement
# https://itsfoss.com/things-to-do-after-installing-ubuntu-18-04/
# sudo apt install tlp tlp-rdw
# sudo tlp start

# Examples
## https://github.com/voku/dotfiles/blob/master/firstInstallDebianBased.sh

## https://github.com/retorquere/zotero-deb
# wget -qO- https://github.com/retorquere/zotero-deb/releases/download/apt-get/install.sh | sudo bash
# sudo apt update
# sudo apt install zotero

sudo apt install -y gdu;
## Gdu – Linux Disk Usage Analyzer

