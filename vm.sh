#Docker
## https://medium.com/axon-technologies/installing-a-windows-virtual-machine-in-a-linux-docker-container-c78e4c3f9ba1

sudo apt-get update;
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common;
curl -fSSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add ;
sudo apt-key fingerprint 0EBFCD88;
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable";
sudo apt update;
sudo apt install docker-ce -y;
sudo systemctl start docker;
sudo systemctl enable docker;

# Multipass
## https://github.com/canonical/multipass
sudo snap install multipass;
