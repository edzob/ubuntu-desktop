#!/bin/bash


if [ ! -x /usr/bin/nc ] ; then
    # some extra check if wget is not installed at the usual place                                                                           
    command -v nc >/dev/null 2>&1 || { echo >&2 "Please install nc or set it in your path. Aborting."; exit 1; }
fi
# https://stackoverflow.com/questions/14411103/check-for-existence-of-wget-curl


if [ ! -x /usr/bin/openssl ] ; then
    # some extra check if wget is not installed at the usual place                                                                           
    command -v openssl >/dev/null 2>&1 || { echo >&2 "Please install openssl or set it in your path. Aborting."; exit 1; }
fi


test=google.com

if (nc -zw1 $test 443) && (echo |openssl s_client -connect $test:443) 2>&1 |awk '
  handshake && $1 == "Verification" { if ($2=="OK") exit; exit 1 }
  $1 $2 == "SSLhandshake" { handshake = 1 }'
then
  echo "we have connectivity"
fi

#https://unix.stackexchange.com/questions/190513/shell-scripting-proper-way-to-check-for-internet-connectivity