# try

## download mainline kernel script

```bash
wget https://raw.githubusercontent.com/pimlie/ubuntu-mainline-kernel.sh/master/ubuntu-mainline-kernel.sh;

```

## re-install firmware 
```bash
sudo apt --reinstall install linux-firmware;
```

## update micro code
```bash
sudo apt install intel-microcode;

```

## install firmware non-free
```bash
sudo apt install firmware-misc-nonfree
```

## upgrade firmware
```bash
sudo fwupdmgr update
```

## Current running Kernel
```bash
uname -r;
uname -a;
sudo lsb_release -a;

dpkg-query --list linux-firmware;

inxi -Fazy;
inxi -Fxxxza;
lspci -knn | grep Net -A3; rfkill list; 
sudo lspci -nn | grep Qualcomm;
hwinfo | grep Wireless;
device list;

dmesg | grep ath11k;
sudo lsmod | grep ath11k;

sudo dmesg | grep firmware;
sudo dmesg | grep microcode;

```

## bios
` As for the detection in the BIOS, make sure secureboot is disabled and/or try enabling or disabling CSM mode`
1. https://bbs.archlinux.org/viewtopic.php?id=267147


## grub
1. https://wiki.ubuntu.com/Kernel/KernelBootParameters
1. https://askubuntu.com/questions/19486/how-do-i-add-a-kernel-boot-parameter


`intel_iommu=on`
1. https://bbs.archlinux.org/viewtopic.php?id=262307

`memmap=12M\\\$20M`
1. https://bbs.archlinux.org/viewtopic.php?id=262307
1. https://bugzilla.kernel.org/show_bug.cgi?id=210923
1. https://bugs.archlinux.org/task/69223
1. https://lore.kernel.org/ath11k/1605121102-14352-1-git-send-email-kvalo@codeaurora.org/
1. https://github.com/0day-ci/linux/commit/e2c4c01cee2e8ce50345b8f70f192921a4875e18
1. https://github.com/0day-ci/linux/commit/e2c4c01cee2e8ce50345b8f70f192921a4875e18
1. https://wiki.archlinux.org/title/Dell_XPS_13_(9310)#AX500
1. https://bbs.archlinux.org/viewtopic.php?id=264268

 `git clone -b ath11k-qca6390-bringup https://git.kernel.org/pub/scm/linux/kernel/git/kvalo/ath.git`
 1. https://medium.com/@tomas.heiskanen/dell-xps-15-9500-wifi-on-ubuntu-20-04-d5f1c218e78a
1. https://lkml.org/lkml/2020/9/14/964
1. https://askubuntu.com/questions/1131885/help-w-possible-missing-firmware-lib-firmware-i915-kbl-guc-ver9-14-bin-for-mo


`git clone https://git.kernel.org/pub/scm/linux/kernel/git/kvalo/ath.git/tag/?h=ath11k-qca6390-bringup-202012140938`
1. https://mukaiguy.com/adding-wifi-drivers-to-ubuntu-linux-for-dells-xps-15-9500-with-ax500-f535fb42db70

 `git clone https://github.com/kvalo/ath11k-firmware.git`
 1. https://medium.com/@tomas.heiskanen/dell-xps-15-9500-wifi-on-ubuntu-20-04-d5f1c218e78a
1. https://lkml.org/lkml/2020/9/14/964

 `build kernel with ath11 module`
1. https://medium.com/@tomas.heiskanen/dell-xps-15-9500-wifi-on-ubuntu-20-04-d5f1c218e78a
1. https://lkml.org/lkml/2020/9/14/964
1. https://mukaiguy.com/adding-wifi-drivers-to-ubuntu-linux-for-dells-xps-15-9500-with-ax500-f535fb42db70

`# sudo apt install linux-oem-20.04-edge # installs kernel version 5.10.0-1008-oem`
1. https://askubuntu.com/questions/1280328/ubuntu-20-04-killer-ax500s-dbs-drivers-support

```bash
git clone https://github.com/kvalo/ath11k-firmware.git
sudo mkdir -p /lib/firmware/ath11k/QCA6390/hw2.0/;
sudo cp QCA6390/hw2.0/WLAN.HST.1.0.1-01740-QCAHSTSWPLZ_V2_TO_X86-1/*.bin /lib/firmware/ath11k/QCA6390/hw2.0/;
sudo cp QCA6390/hw2.0/WLAN.HST.1.0.1-01740-QCAHSTSWPLZ_V2_TO_X86-1/bdwlan.e04 /lib/firmware/ath11k/QCA6390/hw2.0/board.bin;
```
1. https://askubuntu.com/questions/1280328/ubuntu-20-04-killer-ax500s-dbs-drivers-support
1. https://askubuntu.com/questions/1131885/help-w-possible-missing-firmware-lib-firmware-i915-kbl-guc-ver9-14-bin-for-mo
1. https://forum.manjaro.org/t/dell-xps-15-9500-killer-wi-fi-6-ax500-dbs-driver/22159/10



# Dell XPS 9500 (new) i9 - Wifi not working on Ubuntu Linux.

Like many I have had the issue with the wifi not working on my 
2021 Dell XPS 9500 (new) with the i9 and 64GB ram.

When I installed ubuntu 20.04 wifi did not work, 
when I updated to 20.10 wifi did not work. 

When I did a clean install of ubuntu 21.04 wifi worked for two reboots, 
and then stopped working. Not even when installing the newest 5.12.6 kernel.
After a lot of google, and trying I think the steps below in the end worked.

Also the Dell needs some other love to get ubuntu installed (step 0), 

# (0) BIOS
1. use F2 when the dell logo is shown, to enter the bios.
1. disable RAID and switch to AHCI, since ubuntu and Intel Rapid Storage Technology (RST) do not work together ([intel-rest]).
1. disable secure boot so that usb boot is possible.

[intel-rst]: https://discourse.ubuntu.com/t/ubuntu-installation-on-computers-with-intel-r-rst-enabled/15347



# (1) Clean install Ubuntu 21.04 from usb stick;
1. download ubuntu 21.04.

1. download usb sketch tool.
```bash
sudo apt update;
sudo apt upgrade -y;
sudo apt install usb-creator-gtk;
```
https://askubuntu.com/questions/952385/how-can-i-install-startup-disk-creator-on-ubuntu-17-04


1. install ubuntu on an usb stick.
1. make certain your dell is connected to a working ethernet cable.
1. use F12 when the dell logo is shown, to see a boot device menu.
1. boot from your ubuntu stick.
1. do a minimal install (or not)
1. at step 5 - Partition the drive - do select advance feature and select encrypted partition (always good to do this)
1. select / enable thirt party drivers

See also https://wiki.archlinux.org/title/Dell_XPS_17_(9700)

# (2) Update linux firmware to support i9 and ath11;

```bash
sudo apt update; 
sudo apt install linux-firmware;
```

# (3) Update ath11 firmware files;


# Change kernel boot parameters in grub;



# Install 5.11.19 kernel;

## mainline kernel
1. https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.11.19/
1. https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.12.6/
1. https://github.com/trustin/linux510-qca6390


```bash
cd /tmp;
mkdir 5.11.19;
cd 5.11.19;
wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.11.19/amd64/linux-headers-5.11.19-051119-generic_5.11.19-051119.202105071131_amd64.deb;
wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.11.19/amd64/linux-headers-5.11.19-051119_5.11.19-051119.202105071131_all.deb;
wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.11.19/amd64/linux-image-unsigned-5.11.19-051119-generic_5.11.19-051119.202105071131_amd64.deb;
wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.11.19/amd64/linux-modules-5.11.19-051119-generic_5.11.19-051119.202105071131_amd64.deb;
sudo dpkg -i *.deb;
```
Reboot your system.

## Computer 
- AMD video not from sleep. 
- Kernel commands are added.
- kernel 5.12.6 did not come from seel
- kernel 5.11.19 did have video issues.


## Microcode & Firmware
1. https://wiki.debian.org/Microcode
1. https://www.cyberciti.biz/faq/install-update-intel-microcode-firmware-linux/

## Ryzen Code
1. https://wiki.archlinux.org/title/Ryzen

## linux-firmware update
1. https://unix.stackexchange.com/questions/556946/possible-missing-firmware-lib-firmware-i915-for-module-i915

## to be sorted
1. https://www.phoronix.com/scan.php?page=article&item=amd-ryzen-znver1&num=1
1. https://wiki.gentoo.org/wiki/AMDGPU
1. https://wiki.gentoo.org/wiki/Ryzen#Random_reboots_with_mce_events

## AK11 / AX500 help asked - no solution
1. https://www.reddit.com/r/Dell/comments/jolmx2/dell_xps_9500_core_i9_wifi_linux_compatibility/
1. https://www.reddit.com/r/DellXPS/comments/mma48c/xps_15_9500_i9_broken_wlan_wireless_adapter/


## ath11K information:
1. https://wireless.wiki.kernel.org/en/users/drivers/ath11k


```bash
# Machine:   Type: Laptop System: Dell product: XPS 15 9500 v: N/A serial: <filter> Chassis: type: 10 serial: <filter> 
# CPU:       Info: 8-Core model: Intel Core i9-10885H bits: 64 type: MT MCP arch: Comet Lake rev: 2 cache: L2: 16 MiB 
# Graphics:  Device-1: Intel CometLake-H GT2 [UHD Graphics] vendor: Dell driver: i915 v: kernel bus-ID: 00:02.0 
#            Device-2: NVIDIA TU117M [GeForce GTX 1650 Ti Mobile] vendor: Dell driver: nvidia v: 470.74 bus-ID: 01:00.0 
# Network:   Device-1: Qualcomm QCA6390 Wireless Network Adapter [AX500-DBS ] vendor: Rivet Networks driver: ath11k_pci 

$ sudo lspci -nn | grep Qualcomm;
6c:00.0 Network controller [0280]: Qualcomm QCA6390 Wireless Network Adapter [AX500-DBS (2x2)] [17cb:1101] (rev 01)

$ hwinfo | grep Wireless;
  E: ID_MODEL_FROM_DATABASE=QCA6390 Wireless Network Adapter [AX500-DBS (2x2)]
  E: ID_MODEL_FROM_DATABASE=QCA6390 Wireless Network Adapter [AX500-DBS (2x2)]

$ lspci -knn | grep Net -A3; rfkill list; 
6c:00.0 Network controller [0280]: Qualcomm QCA6390 Wireless Network Adapter [AX500-DBS (2x2)] [17cb:1101] (rev 01)
  Subsystem: Rivet Networks QCA6390 Wireless Network Adapter [AX500-DBS (2x2)] [1a56:a501]
  Kernel driver in use: ath11k_pci
  Kernel modules: ath11k_pci
6d:00.0 Unassigned class [ff00]: Realtek Semiconductor Co., Ltd. RTS5260 PCI Express Card Reader [10ec:5260] (rev 01)
0: hci0: Bluetooth
  Soft blocked: no
  Hard blocked: no
1: phy0: Wireless LAN
  Soft blocked: no
  Hard blocked: no

$ sudo lsmod | grep ath11k;
ath11k_pci             28672  0
ath11k                425984  1 ath11k_pci
qmi_helpers            28672  1 ath11k
mac80211             1036288  1 ath11k
cfg80211              897024  2 ath11k,mac80211
mhi                    73728  2 ath11k_pci,qrtr_mhi


$ sudo lsb_release -a;
No LSB modules are available.
Distributor ID: Ubuntu
Description:  Ubuntu Impish Indri (development branch)
Release:  21.10
Codename: impish

$ uname -r;
5.14.7-051407-generic

$ uname -a;
Linux edzob-XPS-15-9500 5.14.7-051407-generic #202109221210 SMP Wed Sep 22 15:15:48 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux

 dpkg-query --list linux-firmware;
Desired=Unknown/Install/Remove/Purge/Hold
| Status=Not/Inst/Conf-files/Unpacked/halF-conf/Half-inst/trig-aWait/Trig-pend
|/ Err?=(none)/Reinst-required (Status,Err: uppercase=bad)
||/ Name           Version      Architecture Description
+++-==============-============-============-=================================
ii  linux-firmware 1.200        all          Firmware for Linux kernel drivers

$ sudo dmesg | grep firmware;
[    1.574305] i915 0000:00:02.0: [drm] Finished loading DMC firmware i915/kbl_dmc_ver1_04.bin (v1.4)
[    1.842660] psmouse serio1: elantech: assuming hardware version 4 (with firmware version 0x0f5002)


$ dpkg-query --list intel-microcode;
Desired=Unknown/Install/Remove/Purge/Hold
| Status=Not/Inst/Conf-files/Unpacked/halF-conf/Half-inst/trig-aWait/Trig-pend
|/ Err?=(none)/Reinst-required (Status,Err: uppercase=bad)
||/ Name            Version             Architecture Description
+++-===============-===================-============-===========================================
ii  intel-microcode 3.20210608.2ubuntu1 amd64        Processor microcode firmware for Intel CPUs

$ sudo dmesg | grep microcode;
[    1.110002] microcode: sig=0xa0652, pf=0x20, revision=0xea
[    1.110356] microcode: Microcode Update Driver: v2.2.



```