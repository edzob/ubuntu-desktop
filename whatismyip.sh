#!/bin/bash 
### This script provides your local IP and your external IP and the status of your nordvpn status



#################################################################
## Check internet connection

### Check install WGET
#### https://stackoverflow.com/questions/14411103/check-for-existence-of-wget-curl
if [ ! -x /usr/bin/wget ] ; then
    # some extra check if wget is not installed at the usual place                                                                           
    command -v wget >/dev/null 2>&1 || { echo >&2 "Please install wget or set it in your path. Aborting."; exit 1; }
else
    ### Check connection
    #### https://stackoverflow.com/questions/929368/how-to-test-an-internet-connection-with-bash
    wget -q --spider http://google.com

    if [ $? -eq 0 ]; then
        echo "You are Online"
    else
        echo "You are Offline"; exit;
    fi
fi

#################################################################



#################################################################
## Check install nordvpn
### https://stackoverflow.com/questions/14411103/check-for-existence-of-wget-curl
if [ ! -x /usr/bin/nordvpn ] ; then
    #some extra check if wget is not installed at the usual place                                                                           
    command -v nordvpn >/dev/null 2>&1 || { echo >&2 "Please install nordvpn or set it in your path. ";}
else
    nordvpn status;
fi
#################################################################



#################################################################
## Get IP address
### https://stackoverflow.com/questions/13322485/how-to-get-the-primary-ip-address-of-the-local-machine-on-linux-and-os-x
### https://www.cyberciti.biz/faq/how-to-find-my-public-ip-address-from-command-line-on-a-linux/

### Check install ip
#### https://stackoverflow.com/questions/14411103/check-for-existence-of-wget-curl
if [ ! -x /usr/bin/ip ] ; then
    # some extra check if wget is not installed at the usual place                                                                           
    command -v ip >/dev/null 2>&1 || { echo >&2 "Please install ip or set it in your path. Aborting."; exit 1; }
fi
# ifconfig is deprected #https://stackoverflow.com/questions/53215483/how-do-i-find-out-the-ip-address-of-my-crostini-container
# ip_local() { #internal IP from this device. wlo1 is an example of the interface
#     ifconfig wlo1 | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p'
#}
ip_local() { #internal IP from this device. wlo1 is an example of the interface
         ip addr | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p'
}


### Check install dig
#### https://stackoverflow.com/questions/14411103/check-for-existence-of-wget-curl
if [ ! -x /usr/bin/dig ] ; then
    # some extra check if wget is not installed at the usual place                                                                           
    command -v dig >/dev/null 2>&1 || { echo >&2 "Please install dig or set it in your path. Aborting."; exit 1; }
fi
ip_external() { #external IP from this device
    dig +short myip.opendns.com @resolver1.opendns.com;
}

LOCALIP="$(ip_local)"
EXTERNALIP="$(ip_external)"

echo "Local    IP $LOCALIP"
echo "External IP $EXTERNALIP"
#################################################################

