# https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.11/amd64/
# https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.11.15/

# https://ubuntuhandbook.org/index.php/2020/08/mainline-install-latest-kernel-ubuntu-linux-mint/
# https://ubuntuhandbook.org/index.php/2020/12/install-linux-kernel-5-10-ubuntu-linux-mint/
# https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.11.16/

cd /tmp/;
mkdir "5.11.16";
cd "/tmp/5.11.16";


# gemeric headers
wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.11.16/amd64/linux-headers-5.11.16-051116-generic_5.11.16-051116.202104211235_amd64.deb;

# all headers
wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.11.16/amd64/linux-headers-5.11.16-051116_5.11.16-051116.202104211235_all.deb;

# unsigned kernel image
wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.11.16/amd64/linux-image-unsigned-5.11.16-051116-generic_5.11.16-051116.202104211235_amd64.deb;

# MOdules
wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.11.16/amd64/linux-modules-5.11.16-051116-generic_5.11.16-051116.202104211235_amd64.deb;
=======

# https://wiki.ubuntu.com/Kernel/MainlineBuilds

cd /tmp/;
mkdir "5.11.rt";
cd "/tmp/5.11.rt";


# ALL headers
wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.11.16/amd64/linux-headers-5.11.16-051116-lowlatency_5.11.16-051116.202104211235_amd64.deb;

# GENERIC headers
wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.11.16/amd64/linux-headers-5.11.16-051116_5.11.16-051116.202104211235_all.deb;

# unsigned kernel image
wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.11.16/amd64/linux-image-unsigned-5.11.16-051116-lowlatency_5.11.16-051116.202104211235_amd64.deb;

# MOdules
wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.11.16/amd64/linux-modules-5.11.16-051116-lowlatency_5.11.16-051116.202104211235_amd64.deb;

sudo dpkg -i *.deb;

# sudo dpkg --purge linux-image-unsigned-5.11.0-051000-generic

# copy AMD lib files
# https://itectec.com/ubuntu/ubuntu-missing-firmware-for-amdgpu/

