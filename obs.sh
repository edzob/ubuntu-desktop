#!/bin/bash 

# How to install OBS

## https://obsproject.com/wiki/install-instructions#linux
sudo apt install ffmpeg;
sudo add-apt-repository ppa:obsproject/obs-studio;
sudo apt update;
sudo apt install obs-studio;

## https://obsproject.com/forum/threads/obs-virtual-cam-on-linux.134849/
sudo apt install v4l2loopback-dkms;

## https://github.com/umlaeute/v4l2loopback/wiki/OBS-Studio
sudo modprobe v4l2loopback devices=1 video_nr=10 card_label="OBS Cam" exclusive_caps=1;


v4l2-ctl --list-devices;
sudo apt-get install v4l-utils

## https://jonathanbossenger.com/obs-studio-linux-virtual-camera/
## https://thesquareplanet.com/blog/camera-webcam-on-linux/

## https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1932367
## https://thesquareplanet.com/blog/camera-webcam-on-linux/
## https://www.reddit.com/r/linuxhardware/comments/dzqmvq/did_anyone_tried_an_elgato_cam_link_4k_on_gnulinux/

## https://github.com/jkulesza/usbreset
## https://wiki.archlinux.org/title/webcam_setup
## https://github.com/xkahn/camlink

## https://jonathanbossenger.com/2020/12/17/obs-studio-linux-virtual-camera/
sudo snap connect obs-studio:kernel-module-observe
sudo apt -y install v4l2loopback-dkms v4l2loopback-utils
echo “options v4l2loopback devices=1 video_nr=13 card_label=’OBS Virtual Camera’ exclusive_caps=1” | sudo tee /etc/modprobe.d/v4l2loopback.conf
echo “v4l2loopback” | sudo tee /etc/modules-load.d/v4l2loopback.conf
sudo modprobe -r v4l2loopback
sudo modprobe v4l2loopback devices=1 video_nr=13 card_label=’OBS Virtual Camera’ exclusive_caps=1

## https://github.com/AdamGleave/elgato-camlink-workaround
## https://github.com/umlaeute/v4l2loopback#run

## https://ubuntuforums.org/showthread.php?t=2444854
v4l2-ctl --list-devices;
sudo modprobe v4l2loopback
ffmpeg -f v4l2 -framerate 50 -pix_fmt yuyv422 -video_size 1920x1080 -i /dev/video0 -f v4l2 -vcodec rawvideo -pix_fmt yuv420p /dev/video2
