#!/bin/bash 

echo "# Version information";
echo "#################################################";
echo "## Laptop/Desktop information";
echo "#################################################";
sudo dmidecode | grep -A 9 "System Information";
sudo dmidecode | grep 'SKU Number' | head -1;


echo "#################################################";
echo "## CPU information";
echo "#################################################";
grep 'model name' /proc/cpuinfo |head -n1 && lspci;

echo "#################################################";
echo "## Linux Kernel information";
echo "#################################################";
cat /proc/version;

echo "#################################################";
echo "## Linux Distro information";
echo "#################################################";
cat /etc/*release;
sudo lsb_release -a;
uname -r;

echo "#################################################";
echo "## USB internals information";
echo "#################################################";
lsusb;

echo "#################################################";
echo "## Complete system overview information";
echo "#################################################";
sudo apt install inxi -y;
inxi -Fxxxrz;


echo "# Linux Update";
echo "#################################################";
echo "## Clean system before upgrade";
echo "#################################################";
sudo apt-get clean -yq;
sudo apt-get autoclean -yq;
sudo apt-get autoremove -yq;

echo "#################################################";
echo "## upgrade SNAP";
echo "#################################################";
sudo snap refresh;

echo "#################################################";
echo "## upgrade APT";
echo "#################################################";
sudo apt-get update;
sudo apt-get upgrade -y;
sudo apt-get dist-upgrade -y;

echo "#################################################";
echo "## Clean system after upgrade";
echo "#################################################";
sudo apt-get clean -yq;
sudo apt-get autoclean -yq;
sudo apt-get autoremove -yq;

echo "#################################################";
echo "## Firmware upgrade";
echo "#################################################";
# sudo fwupdmgr refresh;
sudo fwupdmgr get-upgrades;
sudo fwupdmgr upgrade;

echo "# Speedtest";
curl -s https://install.speedtest.net/app/cli/install.deb.sh | sudo bash;
sudo apt-get install speedtest;
speedtest;

#sudo do-release-upgrade;
