## Gitlab WIKI Markdown
### https://docs.gitlab.com/ee/ssh/README.html
### https://linuxize.com/post/how-to-install-ruby-on-debian-9/
### https://www.svennd.be/gem-command-not-found/

sudo apt install -yqq curl g++ gcc autoconf automake ;
sudo apt install -yqq bison libc6-dev libffi-dev libgdbm-dev libncurses5-dev; 
sudo apt install -yqq libsqlite3-dev libtool libyaml-dev make pkg-config ;
sudo apt install -yqq sqlite3 zlib1g-dev libgmp-dev libreadline-dev libssl-dev;
sudo apt-get install -yqq ruby-dev;
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB;
curl -sSL https://get.rvm.io | bash -s stable;
source ~/.rvm/scripts/rvm;
rvm install 2.5.1;
rvm use 2.5.1 --default; 
sudo apt-get install -yqq rubygems;
sudo gem update;
sudo gem install gollum;
sudo gem install github-markdown;

## Docker
### https://docs.docker.com/engine/installation/linux/docker-ce/debian/#install-using-the-repository
sudo apt-get remove docker docker-engine docker.io;
sudo apt-get update;
sudo apt-get install apt-transport-https  ca-certificates curl gnupg2 software-properties-common;
curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | sudo apt-key add -;
sudo apt-key fingerprint 0EBFCD88;
## 9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable";
sudo apt-get update ; sudo apt-get install docker-ce;
sudo systemctl enable docker;
sudo docker run hello-world;
sudo groupadd docker;
sudo usermod -aG docker $USER;
docker run hello-world;

## Docker compose
### https://docs.docker.com/compose/install/
sudo curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose;
sudo chmod +x /usr/local/bin/docker-compose;
docker-compose --version;


# Unetbootin 
sudo add-apt-repository ppa:gezakovacs/ppa;

sudo apt-get remove docker docker-engine docker.io;
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -;
echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list;

# install software
sudo apt-get install -yqq unetbootin 
sudo apt-get install -yqq  code code-insiders terminology;
sudo apt-get install -yqq docker-ce docker-compose ;

## Power Statistics
[Power Statistics](https://www.omgubuntu.co.uk/2013/12/check-battery-life-health-ubuntu-linux)
* ‘Energy when full‘ (your battery’s current max charge) 
* ‘Energy (design)‘ (the original maximum charge specified by the manufacturer).

## Upower
`upower -i /org/freedesktop/UPower/devices/battery_BAT1`

`acpi`

`acpi -ib`

* [upower command](https://askubuntu.com/questions/888207/battery-health-in-ubuntu-14-04)
* [upower acpi](https://www.cyberciti.biz/faq/howto-check-health-of-laptop-battery/)
* [upower acpi](https://www.ostechnix.com/how-to-check-laptop-battery-status-in-terminal-in-linux/)

## My Battery - Stats
* energy:              69,344 Wh
* energy-empty:        0 Wh
* energy-full:         69,784 Wh (=> 5815.3333333mAh)
* energy-full-design:  72,6 Wh

## My Battery - specs
* Asus K53SD Accu 6600mAh
* 6600 MAh
* 12V
* 79.2 Wh

## VERSION
lsb_release -a
cat /proc/version
cat /etc/*release

# HARDWARE
lscpu
lshw --short
hwinfo --short
lspci
lspci -nn
lsusb
dmesg
df -H
pydf

# VIDEO
lshw -c video
lspci | grep VGA
glxinfo | grep -i vendor
find /dev -group video
lspci | grep VGA ; lsmod | grep "kms\|drm" ; find /dev -group video ; cat /proc/cmdline ; find /etc/modprobe.d/; cat /etc/modprobe.d/*kms* ; ls /etc/X11/xorg.conf ; glxinfo | grep -i "vendor\|rendering" ; grep LoadModule /var/log/Xorg.0.log

# WIFI
sudo iwlist scan
nmcli dev wifi list
sudo iw dev wlan0 scan | grep SSID
sudo lshw -C network
lspci -vvnn | grep -A 9 Network 
lspci -vnn -d 14e4:
sudo depmod -a
nmcli dev wifi


# WIFI
 sudo apt-get update
 sudo apt-get install wavemon
 wavemon
 sudo iwconfig
 watch -n1 iwconfig

# https://www.linuxjournal.com/content/wi-fi-command-line
# https://askubuntu.com/questions/567006/how-can-i-display-the-list-of-available-wifi-networks
 
# http://www.binarytides.com/linux-commands-hardware-info/
# https://askubuntu.com/questions/686239/how-do-i-check-the-version-of-ubuntu-i-am-running
# https://help.ubuntu.com/community/CheckingYourUbuntuVersion
