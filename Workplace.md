Some requirements for a Home Theater setup,
mixed with some home office bonus requirements. 

# Table of Contents
[[_TOC_]]

# Home Theater Requirements
1. Must Have Requirements
    1. Silent 
    1. Low idle watt power consumption
    1. Ununtu 20.04 compatible
    1. Streaming Video on Demand
        1. Netflix in HD
        1. Disney+ in HD
        1. Amazon Prime in HD
        1. NPO Plus in HD
        1. Youtube in HD
    1. Gmail / Google Keep / Google Docs
    1. NordVPN always-on
1. Should have
    1. Steam Games support
    1. Starcraft 2 support
    1. Low power consumption when not playing games
1. Nice to have
    1. Harddisk space for backups
    2. Multiple VMś

# Case
1. Must have
    1. silent
    1. 4 usb ports
1. Should have
    1. passive cooling
    2. Formfactor: Barebone Small Form Factor (SFF)
    1. Formfactor: HTPC
1. Could Have
    1. VESA Mount option (to put it on the back of the TV) 

## Ethernet
1. Must have
    1. Gigabit 

## CPU
Benchmark is the i7‑8705G (hades) and th i7-10710U (frost)
since those are more then enough for 4K and the work needed.
The Frost comes with an iGPU and is not good for gaming.
The Hades comes with a GPU and is good for gaming.

Cheaper build with relative same idle powerconsumption
than the Hades or Frost is an ASRock Deskmini build
with an Intel (iGPU) or AMD (iGPU, ASRock DeskMini A300).



1. AMD
    1. AMD Ryzen 5 3600 (? core, ? threads) - ti high idle power consumption?
1. Intel Nuc
    1. Hades
        1. i7‑8705G (? core, ? threads)(NUC8i7HNK) 
        2. i7‑8809G (? core, ? threads)(NUC8i7HVK) 
    1. Ghost
        1. i7-9750H 
    1. Frost
        1. 	Intel Core i7-10710U 

## GPU
1. Must Have
    1. Linux (kernel) drivers 
    2. HDMI 2.0 4K@ 60Hz
1. Intel Nuc
    1. Hades
        1. HD Graphics 630 
        1. Radeon RX Vega M GL	 
    1. Ghost
        1. Intel® UHD Graphics 630
1. Debian support example
    1. Sapphire 11268-09-20G grafische kaart ATI Radeon Rx 550 1206 MHz PCI Express
1. Examples with AMD Ryzen 5 3600
    1. GeForce GTX 1070 8 GB XLR8 Video Card ([portablegamepc](https://de.pcpartpicker.com/b/vr7TwP))

## RAM
1. Must have
    1. 16 GB
1. Should have and upgradable
    1. 32GB and upgradable 
1. Probably smart (for integrated GPU)
    1. 32GB -> 2x 16 GB DDR4 3000MHz

## Storage (SDD/HDD)
1. Must have
    1. Primairy SDD  
1. Should have
    1. extra HDD (4Tb for backups) 
    
# Home office Requirements

## Keyboard

## Mouse
1. Must have
    1. Logitech OR Microsoft
    1. silent scroll-weel
1. Should have
    1. horizontal scroll/pane

## Monitor
1. Must Have
    1. Anti-glare
    1. HDMI
1. Should Have
    1. USB-C
1. Could Have
    1. 4K
    1. Landscape rotation
1. Nice reviews
    1. BenQ EW3270OU (Klaas)
    1. BenQ PD2700U (Alex Ellis) 

## Webcam
1. Must have
    1. Logitech

## Microphone
1. Nice reviews
    1. Zoom H6
    2. Audio-Technica AT2020+ Cardioid Condenser USB Microphone

# Sources

## Buildguides
1. [Klaas: Switching from a Mac mini 2012 to an Intel NUC8i5BEK and Ubuntu - Sep 2020](https://medium.com/@pythonpow/switching-from-a-mac-mini-2012-to-an-intel-nuc8i5bek-and-ubuntu-f7a83d362cf0)
1. [Alex Ellis: Building a Linux Desktop for Cloud Native Development - Feb 2020](https://blog.alexellis.io/building-a-linux-desktop-for-cloud-native-development/)
1. [Average Linux User: Linux PC Build 2020 - Mar 2020](https://averagelinuxuser.com/linux_pc_assembly/)
1. [The Tech buyer's guru: The Best $650/$900 Intel NUC Ultra-Compact PC Builds - APRIL 2020](https://techbuyersguru.com/intel-nuc-ultra-compact-pc-build)
1. [The Tech buyer's guru: The best $1,000 High-End 4K Home Theater PC Build - Jun 2020](https://techbuyersguru.com/1000-high-end-4k-home-theater-pc-build)
1. [Delightly Linux: Intel NUC, Linux, Pi-Hole, and NAS – Part 1: The Hardware and Xubuntu 19.10 - Nov 2019](https://delightlylinux.wordpress.com/2019/11/02/intel-nuc-linux-pi-hole-and-nas-part-1-the-hardware-and-xubuntu-19-10/)
1. [Current Build: Dream PC - Powerful or gaming computer - Apr 2020](https://www.currentbuild.com/dream-computer-1.php) 
1. [Current Build: Sense mini PC 2 - Premium mainstream mini computer - Mar 2020](https://www.currentbuild.com/dream-computer-1.php) 
1. [Reddit: Build for a linux friendly decent gaming PC - May 2020](https://www.reddit.com/r/linuxhardware/comments/g6iwod/build_for_a_linux_friendly_decent_gaming_pc/)
2. [PC Part Picker: Entry Level AMD Gaming Build](https://de.pcpartpicker.com/guide/FNwrxr/entry-level-amd-gaming-build)
1. [PC Part Picker: First Time Builder](https://de.pcpartpicker.com/b/DLJbt6)
1. [Hardware Revolution: Build your Budget Gaming PC: Spring 2020](https://www.hardware-revolution.com/best-low-cost-budget-gaming-pc-april-2020/) 
1. [Tweakers: Stille game PC 1440p 1000 Euro - May 2020](https://gathering.tweakers.net/forum/list_messages/1996402) 
1. [Tweakers: ITX Development PC voor op werk 737 Euro - May 2020](https://gathering.tweakers.net/forum/list_messages/1996572) 
2. [Tweakers: Allround PC voor ongeveer €700](https://gathering.tweakers.net/forum/list_messages/1996096)
3. [PC Part Picker: complete builds ASRock Deskmini A300 Ryzen 5 3400G](https://pcpartpicker.com/builds/by_part/sQprxr)

## Comparison and News Sites
1. [Tom's Guide: The best mini PCs in 2020 - May 2020](https://www.tomsguide.com/us/best-mini-pcs,review-2760.html)
1. [It's Foss: 11 Mini PCs That Come With Linux Pre-installed - May 2020](https://itsfoss.com/linux-based-mini-pc/)
1. [Tweakers.net: Desktop Best Buy Guide Hometheater-pc - May 2020](https://tweakers.net/reviews/7834/4/desktop-best-buy-guide-mei-2020-hometheater-pc.html)
1. [Fanless Tech](https://www.fanlesstech.com/)
1. [TechRepublic: 2018 Mac Mini blocks Linux, here are alternative small form factor PCs - Nov 2018](https://www.techrepublic.com/article/2018-mac-mini-blocks-linux-here-are-alternative-small-form-factor-pcs/)
1. [Colour my learning: Best Mini PC – the Future is Small & Compact - Jan 2018](https://www.colourmylearning.com/2018/01/best-mini-pc-the-future-is-small-compact/6/)
1. [Test IT.de: Mini PC Test 2020- May 2020](https://www.testit.de/tag/mini-pc.html) 
1. [Heise.de: Thema Mini pc](https://www.heise.de/thema/Mini_PC)
1. [Logical Increments: How to Build the Best Living Room Gaming PC - May 2020](https://www.logicalincrements.com/articles/build-pc-living-room-gaming-steam-machine)
1. [Build-Gaming-Computers: Plan the Best Video Editing PC Build](https://www.build-gaming-computers.com/video-editing-pc-build.html)
1. [Tuxedo Computers: Mini Systems - as small as possible](https://www.tuxedocomputers.com/en/Linux-Hardware/Linux-Computer-/-PCs/Mini-Systeme.tuxedo)
1. [Tech Power UP: AMD Ryzen 5 3600 Review - power consumptiojn](https://www.techpowerup.com/review/amd-ryzen-5-3600/18.html)
1. [PC Mag: The Best Windows Mini PCs for 2020 - Apr 2020](https://www.pcmag.com/picks/the-best-windows-mini-pcs )
1. [AnandTech: Intel NUC10i7FNH Frost Canyon Review and ASRock - Mar 2020](https://www.anandtech.com/show/15571/intel-nuc10i7fnh-frost-canyon-review/9)
1. [Tech Power Up: ASRock DeskMini A300 (Ryzen 5 2400G) Review - Apr 2019](https://www.techpowerup.com/review/asrock-deskmini-a300/10.html)
1. [ASRock: Deskmini A300 Specs](https://www.asrock.com/nettop/AMD/DeskMini%20A300%20Series/)
1. [ASRock: Deskmini Specs](https://www.asrock.com/nettop/Intel/DeskMini%20310%20Series/index.asp)
1. [Crucial: ASRock Deskmini A300 Compatible memory](https://www.crucial.com/compatible-upgrade-for/asrock/deskmini-a300)
1. [PC-Builds: AMD Ryzen 5 PRO 3400GE vs AMD Ryzen 5 3400G](https://pc-builds.com/compare/cpu/0Uq/0Vv/)
1. [AMD: AMD Ryzen™ 5 3400G with Radeon™ RX Vega 11 Graphics ](https://www.amd.com/en/products/apu/amd-ryzen-5-3400g)
1. [CPU Benchmark: Ryzen 5 3400G vs Ryzen 5 2400G](https://cpu.userbenchmark.com/Compare/AMD-Ryzen-5-3400G-vs-AMD-Ryzen-5-2400G/m825156vsm433194)

## Pricing overview
1. [Alternate - Barebones](https://www.alternate.de/Barebones)

# Brand and Types

## [Intel NUC's](https://en.wikipedia.org/wiki/Next_Unit_of_Computing)
1. Hades Canyon NUC - Gamer - Mac Mini competitor
    1. [Notebook Check Hades benchmark - mar 2020](https://www.notebookcheck.net/Two-years-later-the-Intel-Hades-Canyon-mini-PC-is-still-the-best-and-fastest-NUC-you-can-get.462485.0.html) 
1. Coffee Lake NUC - SOHO
1. Chaco Canyon NUC - IoT - Rapsberry Pi competitor
1. Frost Canyon
    1. [Notebook CHeck Frost benchmark - feb 2020](https://www.notebookcheck.net/Intel-Frost-Canyon-NUC-offers-6-cores-and-12-threads.453555.0.html) 

1. Hades Canyon = 8th generation
2. Ghost Canyon = 9th generation
3. Frost Canyon = 10th generation (best?)

## [Dell Alienware](https://en.wikipedia.org/wiki/Alienware)
1. Alienware alpha - Games - Mac Mini / Hades Canyon competitor - discontinued

## [Purism](https://puri.sm/posts/announcing-the-purism-librem-mini/)
1. Purisme Libre mini

## System76 EU alternative

## Gigabytes Brix
1. https://www.notebookcheck.com/Gigabyte-Leistungsstarke-NUC-Konkurrenten-mit-Comet-Lake-U-Prozessoren-vorgestellt.454415.0.html
2. https://www.gigabyte.com/us/Mini-PcBarebone

## [AMD](https://www.amd.com/en/products/embedded-minipc-solutions#Media--Enterprise-Mini-PC-)

### Barebone reviews and pricing
1. [NUC8i3BEH2  299 EUR](https://www.alternate.de/Intel/NUC-Kit-NUC8i3BEH2-Barebone/html/productRatings/1477106)
    1. Prozessor: Intel® Core™ i3-8109U
    1. Grafikchip: Intel® Iris Plus Graphics 655 
    2. [specs](https://www.alternate.de/Intel/NUC-Kit-NUC8i3BEH2-Barebone/html/product/1477106?)


