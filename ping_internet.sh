#!/bin/bash

ping -q -c1 google.com &>/dev/null && echo online || echo offline

# https://stackoverflow.com/questions/929368/how-to-test-an-internet-connection-with-bash