#!/bin/bash

if [ ! -x /usr/bin/wget ] ; then
    # some extra check if wget is not installed at the usual place                                                                           
    command -v wget >/dev/null 2>&1 || { echo >&2 "Please install wget or set it in your path. Aborting."; exit 1; }
fi
#https://stackoverflow.com/questions/14411103/check-for-existence-of-wget-curl

wget -q --spider http://google.com

if [ $? -eq 0 ]; then
    echo "Online"
else
    echo "Offline"
fi

# https://stackoverflow.com/questions/929368/how-to-test-an-internet-connection-with-bash