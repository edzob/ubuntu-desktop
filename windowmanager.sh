#!/bin/bash 

# "Shell Tile", "gTile", "Material Shell" as gnome-extension for tile window management
# Variety as wallpapger manager instead of nitrogen for unslpash
# Plank as menubar

sudo apt install variety -y;
sudo apt install plank -y;


## GNOME Extentions
# https://github.com/emasab/shelltile
# https://github.com/gTile/gTile
# https://github.com/material-shell/material-shell/blob/main/README.md

# install Material via 
# https://extensions.gnome.org/extension/3357/material-shell/
# https://extensions.gnome.org/extension/28/gtile/
# https://extensions.gnome.org/extension/657/shelltile/ 

## VARIETY
# https://peterlevi.com/variety/
# https://github.com/varietywalls/variety

## BSPWM
# Great alternative for gnome/material is BSPWM is it blazing fast
# removes Gnome and therefor needs many configuration for network/bluetooth etc
# https://dev.to/l04db4l4nc3r/bspwm-a-bare-bones-window-manager-44di
# https://www.reddit.com/r/bspwm

## REGOLITH
# great other alternative is fast and clean (less minimal als BSWM)
# https://regolith-linux.org/docs/getting-started/basics/
# https://regolith-linux.org/interact/

sudo add-apt-repository ppa:regolith-linux/release
sudo apt install regolith-desktop-standard # or regolith-desktop-mobile for laptops

## POP SHELL
## another alternative is Pop_Shell but this needs npm etc to install
## https://github.com/pop-os/shell

sudo add-apt-repository ppa:peterlevi/ppa
sudo apt update
sudo apt install variety


## PoP OS - how to install different desktop environments
# https://support.system76.com/articles/desktop-environment/

